<?php
session_start();
ini_set('error_prepend_string',"<pre>");
ini_set('error_append_string',"</pre>");


setlocale(LC_TIME, "da_DK");

// Include Header
include 'nem-includes/pages/header.php';


// Check if page is set, if not set page to home
$page = (isset($_GET['page'])) ? $_GET['page'] : 'home';

// Determine which page to display
if (!isset($_SESSION["id"]))
    include "nem-includes/pages/login.php";

elseif($page == 'home')
	include 'nem-includes/pages/home.php';

elseif($page == 'support')
	include 'nem-includes/pages/support.php';

elseif($page == 'ejendomme' && isset($_GET["town"]) && isset($_GET["address"]))
	include 'nem-includes/pages/ejendomme.php';

elseif($page == 'registrer')
    include 'nem-includes/pages/register.php';

elseif($page == 'lejere')
	include 'nem-includes/pages/lejere.php';

elseif($page == 'ejendom')
	include 'nem-includes/pages/ejendom.php';

elseif($page == 'lejer')
	include 'nem-includes/pages/lejer.php';

elseif($page == 'ticket')
	include 'nem-includes/pages/ticket.php';

elseif($page == 'nytsvar')
    include 'nem-includes/pages/support_reply.php';
elseif($page == 'opretsag')
    include 'nem-includes/pages/support_create.php';

elseif($page == 'nyejendom')
    include 'nem-includes/pages/ny_ejendom.php';

elseif($page == 'nyticket')
    include 'nem-includes/pages/ny_ticket.php';

else
    include 'nem-includes/pages/404.php';
	
// Include Footer
include 'nem-includes/pages/footer.php';