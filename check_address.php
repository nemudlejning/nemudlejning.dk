<pre>
<form action="check_address.php" method="post" enctype="multipart/form-data">
    <input type="file" name="files[]" multiple>
    <input type="submit">
</form>

<?php
if (isset($_FILES["files"]))
{
    $dir = "uploads/";

    $files = $_FILES["files"];
    $num = sizeof($_FILES["files"]["name"]); // number of elements uploaded

    $allowedTypes = ["image/png", "image/jpg", "application/pdf", "image/jpeg"];
    for ($i = 0; $i < $num; $i++)
    {
        $error = 0;
        $name = $files["name"][$i];
        $type = $files["type"][$i];

        if (in_array($type, $allowedTypes)) {
            move_uploaded_file($files["tmp_name"][$i], $dir . $files["name"][$i]);
        } else {
            echo "De eneste tilladte filtyper er:";
            foreach ($allowedTypes as $type){
                echo $type;
            }
        }
    }
}