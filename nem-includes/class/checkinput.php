<?php
include "classes.php";
class CheckInput {
    public static function Integer($input) {return is_numeric($input);}
    public static function String($input) {return is_string($input);}

    public static function PropertyType($input) {
        if (!self::String($input))
            return false;

        if (in_array($input, Property::GetTypes()))
            return true;

        return false;
    }

    public static function Sanitize($input)
    {
        $input = trim($input);
        $input = stripslashes($input);
        $input = htmlspecialchars($input);
        return $input;
    }
}