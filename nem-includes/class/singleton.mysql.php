<?php
require_once ('MysqliDb.php');

/**
 * Class Database
 *
 * Singleton MySQLi database implementation
 */
class Database {
    public $db;

    private $servers = array(
        "master" => Array (
            'host' => 'localhost',
            'username' => 'netchatt',
            'password' => '1337Kristian',
            'db'=> 'netchatt_nemudlejning',
            'port' => 3306,
            'prefix' => 'nem_',
            'charset' => 'utf8'),

        "slave1" => Array (
            'host' => 'localhost',
            'username' => 'netchatt',
            'password' => '1337Kristian',
            'db'=> 'netchatt_nemudlejning',
            'port' => 3306,
            'prefix' => 'nem_',
            'charset' => 'utf8'),
    );

    /**
     * @return Database|null
     */
    public static function Instance()
    {
        static $instance = null;
        if ($instance == null)
            $instance = new Database();
        return $instance;
    }

    /**
     * Database constructor.
     */
    private function __construct()
    {
        $this->db = new MysqliDb ($this->servers["master"]);

        $this->db->addConnection("slave", $this->BestSlave());
    }

    /**BestSlave
     * TODO: search for best database slave server
     *
     * @return slave array
     */
    private function BestSlave()
    {
        return $this->servers["slave1"];
    }
}