<?php
require_once 'browser.php';
require_once 'Enums.php';
require_once 'class.feedback.php';
require_once 'user.php';
require_once 'sanitize.php';
require_once 'support.php';
require_once "singleton.mysql.php";
require_once "property.php";
require_once "checkinput.php";
require_once "userinfo.php";
require_once "ticket.php";
require_once "utils.php";