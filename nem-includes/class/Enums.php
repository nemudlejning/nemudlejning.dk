<?php
abstract class Enum
{
    const NONE = null;

    final public static function toArray()
    {
        return (new ReflectionClass(static::class))->getConstants();
    }

    final public static function isValid($value)
    {
        return in_array($value, static::toArray());
    }
}

final class Roles extends Enum
{
    const TENANT    = 1;
    const OWNER     = 2;
    const SUPPORT   = 3;
    const ADMIN     = 4;
}

final class PropertyStatus extends Enum
{
    const FREE = 1;
    const OCCUPIED = 2;
}

final class TicketStatus extends Enum
{
    const CLOSED = 0;
    const OPEN = 1;
    const ANSWERD = 2;
    const TENANTANSWER = 3;
    const HOLD = 4;
}