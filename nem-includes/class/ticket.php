<?php
include "classes.php";
class Ticket {
    public $id, $subject, $time, $status, $conversation;
    private $db;

    public function __construct($ticket_id, $property_id = null)
    {
        $this->db = Database::Instance()->db;

        $bool = (!is_null($property_id));
        $where = ($bool) ? "property" : "id";
        $id = ($bool) ? $property_id : $ticket_id;

        $this->db->where($where, $id);
        $result = $this->db->connection("slave")->get("support");

        if (isset($result[0])){
            foreach ($result as $value){
                $this->subject[]      = $value["subject"];
                $this->id[]           = $value["id"];
                $this->time[]         = $value["time"];
                $this->status[]       = $value["status"];
                $this->tenant[]       = new UserInfo(null, $value["property"]);
            }
            if (!$bool){
                foreach ($this->id as $id){
                    $this->db->where("ticket_id", $id);
                    $this->conversation[] = $this->db->connection("slave")->get("support_tickets");
                }
            }
        }

    }

    public function FirstReply()
    {
        return $this->conversation[0][0];
    }

    public function LastReply()
    {
        return end($this->conversation[0]);
    }


    public static function StatusColor($status)
    {
        if ($status == TicketStatus::CLOSED)
            return "red";
        if ($status == TicketStatus::OPEN)
            return "green";
        if ($status == TicketStatus::ANSWERD)
            return "orange";
        if ($status == TicketStatus::TENANTANSWER)
            return "black";
        if ($status == TicketStatus::HOLD)
            return "blue";

        return "black";
    }

    public static function PrintStatus($status)
    {
        if ($status == TicketStatus::CLOSED)
            return "Lukket";
        if ($status == TicketStatus::OPEN)
            return "Åben";
        if ($status == TicketStatus::ANSWERD)
            return "Besvaret";
        if ($status == TicketStatus::TENANTANSWER)
            return "Nyt svar";
        if ($status == TicketStatus::HOLD)
            return "På hold";
    }

    /**
     * UniqueId
     *
     * returns a unique ticket id
     *
     * @return int
     */
    public static function UniqueId (){
        $id = rand(1e3, 1e6);
        $db = Database::Instance()->db;

        $db->where("id", $id);
        $result = $db->connection("slave")->get("support");

        if (!empty($result))
            return self::UniqueId();

        return $id;
    }

    /**
     * Reply
     *
     * Inserts a new message to 'support_tickets'
     *
     * @param $message
     * @return bool
     */
    public function Reply($message)
    {
        $data = array(
            "ticket_id"     => $this->id[0],
            "respond_id"    => $_SESSION["id"],
            "message"       => $message,
            "time"          => time()
        );

        $this->db->insert("support_tickets", $data);
    }

    public static function Create($subject, $message, $property_id)
    {
        $db = Database::Instance()->db;
        $ticket_id = self::UniqueId();

        $support = array(
            "id"            => $ticket_id,
            "subject"       => $subject,
            "property"      => $property_id,
            "time"          => time(),
            "status"        => TicketStatus::OPEN
        );

        if (!$db->insert("support", $support))
            return false;

        $ticket = new Ticket($ticket_id);
        $ticket->Reply($message);

        return $ticket;
    }
}