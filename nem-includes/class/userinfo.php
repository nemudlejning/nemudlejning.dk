<?php
include "classes.php";
class UserInfo {
    public $id, $name, $role, $email, $phone;
    private $db;
    public function __construct($user_id, $property_id=null)
    {
        $this->db = Database::Instance()->db;

        $bool = (!is_null($property_id));

        // if (!is_null($property_id))
        // Determines which table and where prop to look for
                $id = ($bool) ? $property_id : $user_id;
                $where = ($bool) ? "property" : "id";
                $table = ($bool) ? "property_tenant" : "users";

        $this->db->where($where, $id);
        $data = $this->db->connection("slave")->get($table);

        if (!empty($data)){
            if ($table == "users")
                self::SetData($data);
            elseif ($table == "property_tenant") {
                foreach ($data as $tenant)
                {
                    $this->db->where("id", $tenant["tenant"]);
                    $result = $this->db->connection("slave")->get("users");
                    if (!empty($result))
                        self::SetData($result);
                }
            }
        }
    }

    private function SetData($data)
    {
        foreach ($data as $value){
            $this->id[] = $value["id"];
            $this->name[] = $value["name"];
            $this->role[] = $value["role"];
            $this->email[]= $value["email"];
            $this->phone[] = $value["phone"];
        }
    }

    public function Properties(){
        $properties = [];
        $propid = [];

        $this->db = Database::Instance()->db;
        if ($this->role == Roles::TENANT)
        {
            $this->db->where("tenant", $this->id[0]);
            $result = $this->db->connection("slave")->get("property_tenant");

            $propid[] = $result[0]["property"];
        }else //elseif ($this->role == Roles::OWNER)
        {
            $this->db->where("owner", $this->id[0]);
            $result = $this->db->connection("slave")->get("property");

            foreach ($result as $value){
                $propid[] = $value["id"];
            }
        }

        foreach ($propid as $property){
            $properties[] = new Property($property);
        }

        return $properties;
    }

    public function Tenants()
    {
        $properties = $this->Properties();

        $tenants = [];
        foreach ($properties as $property)
        {
            if ($property->status == PropertyStatus::OCCUPIED)
                $tenants[$property->id] = new self(null, $property->id);
        }

        return $tenants;
    }

    public function SupportTickets() {
        $properties = $this->Properties();

        $tickets = [];
        foreach ($properties as $property){
            if ($property->status == PropertyStatus::OCCUPIED)
                $tickets[$property->id] = new Ticket(null, $property->id);
        }

        return $tickets;
    }


    public function Name()
    {
        if (empty($this->name))
            return "Ingen";

        if (sizeof($this->name) == 1)
            return $this->name[0];

        $str = "";
        foreach ($this->name as $name)
        {
            $str .= $name . "<br>";
        }
        return rtrim($str, '<br>');
    }
    public function Email()
    {
        if (empty($this->email))
            return "";
        if (sizeof($this->email) == 1)
            return $this->email[0];

        $str = "";
        foreach ($this->email as $email)
        {
            $str .= $email . "<br>";
        }
        return rtrim($str, '<br>');
    }

    public function Phone()
    {
        if (empty($this->phone))
            return "";
        if (sizeof($this->phone) == 1)
            return $this->phone[0];

        $str = "";
        foreach ($this->phone as $phone)
        {
            $str .= Utils::PrintPhone($phone) . "<br>";
        }
        return rtrim($str, '<br>');
    }

    public function Role()
    {
        if ($this->role[0] == Roles::TENANT)
            return "Lejer";
        if ($this->role[0] == Roles::OWNER)
            return "Udlejer";
        if ($this->role[0] == Roles::SUPPORT)
            return "Support";
    }
}