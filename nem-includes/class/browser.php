<?php

/**
 * Class Browser
 *
 * Helpfull browser features
 */
class Browser {
    /**
     * Redirect
     *
     * Redirect browser to an "internal" website
     *
     * @param $url
     */
    public static function Redirect($url)
    {
        header("Location: https://www.nemudlejning.dk/".$url);
        die();
    }

    /**
     * ExternalRedirect
     *
     * Redirect browser to an external website
     *
     * @param $url
     */
    public static function ExternalRedirect($url)
    {
        header("Location: ".$url);
        die();
    }

    /**
     * LateRedirect
     *
     * Redirect user after a header is set
     *
     * @param $url
     */
    public static function LateRedirect($url){

        echo '<script> window.location.replace("https://www.nemudlejning.dk/'.$url.'"); </script>';
    }

    public static function Address()
    {
        return $_SERVER["REMOTE_ADDR"];
    }


}
