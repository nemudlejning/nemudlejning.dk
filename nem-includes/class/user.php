<?php
require_once "classes.php";
/**
 * Class User
 */
class User {
    private $email, $name, $phone, $passwd, $salt, $timestamp, $db;

    /**
     * User constructor.
     * @param $email
     * @param $password
     *
     * @return false if user don't pass datacheck
     */
    public function __construct($email, $password)
    {
        $this->db = Database::Instance()->db;

        $this->email = Sanitize($email);
        $this->passwd = Sanitize($password);
        $this->timestamp = $this->Timestamp();
        $this->salt = $this->GenerateSalt();

        if (!$this->DataCheck())
            return false;

        return true;
    }
    public function GetName(){return $this->name;}


    // Logout user
    public static function Logout(){
        session_unset();
        session_destroy();
    }

    // Search email for ID, return 0 if doesn't exist
    public static function GetID($email){
        Database::Instance()->db->where("email", $email);
        $result = Database::Instance()->db->connection("slave")->get("users");
        if ($result && isset($result[0]["id"]))
            return $result[0]["id"];

        return 0;
    }

    /**
     * Generates a timestamp
     *          if user exists -> fetch from DB
     *                    else -> use current time
     *
     * @return int
     */
    private function Timestamp()
    {
        if ($this->Exists()) {
            $this->db->where("email", $this->email);
            $result = $this->db->get('users');
            return $result[0]["timestamp"];
        }

        return time();                                      // else use current time
    }

    /**
     * Generates a salt
     *      Uses email as an int and adds timestamp
     * @return int
     */
    private function GenerateSalt()
    {
        // uses every third char of email and adds timestamp
        $input = $this->email;
        $output = "";

        for ($i = strlen($input) - 1; $i >= 0; $i -= 3) {
            $output .= $input[$i];
        }
        return $output . $this->timestamp;
    }

    /**
     * Check if user exists
     *
     * @return bool
     */
    public function Exists()
    {
        $this->db->where("email", $this->email);
        $result = $this->db->connection("slave")->get('users');
        if ($result)
            return true;

        return false;
    }


    /**
     * Check data for errors
     * @return bool
     */
	private function DataCheck(){
	    //TODO: Special password requirements
		return filter_var($this->email, FILTER_VALIDATE_EMAIL);
	}
	
	// Create new user
	public function Create($name, $phone){
	    if ($this->Exists())
	        return false;

        $this->name = Sanitize($name);
        $this->phone = Sanitize($phone);

        $data = array(
            "name" => $this->name,
            "email" => $this->email,
            "passwd" => $this->Hash(),
            "phone" => $this->phone,
            "ip" => Browser::Address(),
            "timestamp" => $this->Timestamp()
        );

        if ($this->db->insert("users", $data))
            return true;

        return false;
	}

	private function Hash()
    {
        return hash("sha512", $this->passwd . $this->salt);
    }

	// Login user
	public function Login(){
	    $this->db->where("email", $this->email);
	    $result = $this->db->connection("slave")->get("users");

		if (!$result[0])
            return false;

	    $passwd = $this->Hash();
	    if ($result[0]["email"] == $this->email && $result[0]["passwd"] == $passwd){
	        $_SESSION["loggedin"] = true;
	        $_SESSION["id"] = $result[0]["id"];
            return true;
        }


        return false;
	}

    /**
     * External logout
     *
     * Gives administrators the ability to log an user out
     */
    public function ExternalLogout() {
        $this->db->connection("slave")->where("email", $this->email);
        $result = $this->db->connection("slave")->get("users");
	    if ($result[0]["ExternalLogout"])
	        $this->Logout();
    }

	/**
     * Terminate user
     * Toggles terminate in DB
     *
     * @return bool
     */
	public function Terminate()
    {
        $this->db->where("email", $this->email);
        $result = $this->db->connection("slave")->get("users");
        $data = array("terminate" => !$result["terminate"]);
        if ($this->db->insert($data))
            return true;

        return false;
    }
}
