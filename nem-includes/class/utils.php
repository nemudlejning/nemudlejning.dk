<?php
define( TIMEBEFORE_NOW,         'now' );
define( TIMEBEFORE_MINUTE,      '{num} minut siden' );
define( TIMEBEFORE_MINUTES,     '{num} minutter siden' );
define( TIMEBEFORE_HOUR,        '{num} time siden' );
define( TIMEBEFORE_HOURS,       '{num} timer siden' );
define( TIMEBEFORE_YESTERDAY,   'i går' );
define( TIMEBEFORE_FORMAT,      '%e %b' );
define( TIMEBEFORE_FORMAT_YEAR, '%e %b, %Y' );

class Utils {

    public static function time_ago( $time )
    {
        $out    = ''; // what we will print out
        $now    = time(); // current time
        $diff   = $now - $time; // difference between the current and the provided dates

        if( $diff < 60 ) // it happened now
            return TIMEBEFORE_NOW;

        elseif( $diff < 3600 ) // it happened X minutes ago
            return str_replace( '{num}', ( $out = round( $diff / 60 ) ), $out == 1 ? TIMEBEFORE_MINUTE : TIMEBEFORE_MINUTES );

        elseif( $diff < 3600 * 24 ) // it happened X hours ago
            return str_replace( '{num}', ( $out = round( $diff / 3600 ) ), $out == 1 ? TIMEBEFORE_HOUR : TIMEBEFORE_HOURS );

        elseif( $diff < 3600 * 24 * 2 ) // it happened yesterday
            return TIMEBEFORE_YESTERDAY;

        else // falling back on a usual date format as it happened later than yesterday
            return strftime( date( 'Y', $time ) == date( 'Y' ) ? TIMEBEFORE_FORMAT : TIMEBEFORE_FORMAT_YEAR, $time );
    }

    /**
     * Converts postal code to city name via. DAWA
     * @param $postal_code
     * @return mixed
     */
    public static function City($postal_code){
        $url = "https://dawa.aws.dk/postnumre?nr=".$postal_code;
        $jsonData = file_get_contents($url);

        return json_decode($jsonData, true)[0]["navn"];
    }

    public static function VerifyAddress($address)
    {
        $url = "https://dawa.aws.dk/autocomplete?q=" . urlencode($address);

        $jsonDat  = file_get_contents($url);
        $json = json_decode($jsonDat);

        if (isset($json[0]))
            return true;
        else
            return false;
    }

    public static function GetPostalCode($address)
    {
        $url = "https://dawa.aws.dk/autocomplete?q=" . urlencode($address);

        $jsonDat  = file_get_contents($url);
        $json = json_decode($jsonDat);

        if (isset($json[0]))
            return $json[0]["data"]["postnr"];
        else
            return false;

    }
    /**
     * Counts number of time every instance of $col appears
     *
     * @param $table
     * @param $col
     * @param $owner_id
     * @return array
     */
    public static function Count($table, $col, $owner_id){
        $db = Database::Instance()->db;
        $db->where("owner", $owner_id);
        $data = $db->connection("slave")->get($table);
        foreach ($data as $value)
            $status[] = $value[$col];

        return array_count_values($status);
    }

    /**
     * Returns (+45) 00 00 00 00
     *
     * @param $phone    -  00000000
     * @return string   - (+45) 00 00 00 00
     */
    public static function PrintPhone($phone)
    {
        return "(+45) " . chunk_split($phone, 2, ' ');
    }
}


