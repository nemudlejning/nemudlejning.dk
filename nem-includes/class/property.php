<?php
include "classes.php";
class Property {
    public $id, $address, $biography, $status, $area, $rooms, $rent, $town, $type, $owner, $construction_year, $tenant;
    private $db;

    public static function GetTypes() {
        return ["Lejlighed", "Værelse", "Hus"];
    }
    public static function GetStatuses() {
        return ["Udlejet", "Ledig"];
    }

    public function __construct($id)
    {
        $this->db = Database::Instance()->db;

        if (!is_null($id)) {
            $this->db->where("id", $id);
            $data = $this->db->connection("slave")->get("property")[0];

            $this->id                   = $data["id"];
            $this->owner                = $data["owner"];
            $this->address              = $data["address"];
            $this->biography            = $data["biography"];
            $this->status               = $data["status"];
            $this->area                 = $data["area"];
            $this->rooms                = $data["rooms"];
            $this->rent                 = $data["rent"];
            $this->town                 = $data["town"];
            $this->type                 = $data["type"];
            $this->tenant               = new UserInfo(null, $this->id);
            $this->construction_year    = $data["construction_year"];
        }
    }

    /**
     * Checks if a property exists
     *
     * @return bool
     */
    public function Exists(){
        $this->db->where("address", $this->data["address"]);
        $this->db->where("town", $this->data["town"]);
        $result = $this->db->get("property");
        if (isset($result[0]))
            return true;

        return false;
    }

    /**
     * Creates a new property in the database
     *
     * @param $data (has to be same format as database)
     * @return bool
     */
    public function Create($data){
        $this->data = $data;
        if ($this->Exists())
            return false;

        if ($this->db->insert("property", $this->data))
            return true;

        return false;
    }


    public function Status() {
        if($this->status == PropertyStatus::FREE)
            return "Ledig";
        if ($this->status == PropertyStatus::OCCUPIED)
            return "Udlejet";
    }

    /**
     *  Color depends on Status
     *
     * @return string
     */
    public function StatusColor()
    {
        if($this->status == PropertyStatus::FREE)
            return "red";
        if ($this->status == PropertyStatus::OCCUPIED)
            return "green";
    }



    public function Delete(){
        $this->db->where("id", $this->id);
        $this->db->delete("property");

        $this->db->where("property", $this->id);
        $this->db->delete("property_tenant");

        //TODO: Delete support tickets AND ticket headers!
    }
    public function Update(){}
}

