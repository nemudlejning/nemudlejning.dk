<div class="container">
    <div class="float-none col col-4 mx-auto my3">
        <div class="col col-12 box p2 my2">
            <h1 class="text-center font-w1">Registrer en konto</h1>
            <form action="/nem-includes/pages/features/register.php" method="POST" accept-charset="utf-8" class="login">
                <input type="text" name="name" placeholder="Navn" id="name" class="col-10 mx-auto"/>
                <input type="text" name="phone" placeholder="Telefon nummer (+45) XXXXXXXX" id="phone" class="col-10 mx-auto"/>
                <input type="text" name="email" placeholder="Email" id="username" class="col-10 mx-auto"/>
                <input type="password" name="password" placeholder="Adgangskode" id="password" class="col-10 mx-auto"/>
                <input type="submit" name="submit" value="registrer" id="submit" class="mx-auto"/>
            </form>
            <p class="text-center mt3"><a href="/login">Har du allerede et login?</a></p>
        </div>
    </div>
</div>