<div class="container">
    <div class="col col-12 mx-auto my3">
    	<div class="col col-12 box ny_ejendom_header">
    		<div id="mapid" class="col col-12"></div>
    		<div class="col-right col-3 ny_ejendom_img p3">
            	<img src="nem-includes/images/hus.png" alt="" />
            </div>
            
    		<div class="col col-12 p2 ny_ejendom_title">
            	<h1 class="white font-w2">Ny ejendom</h1>
        	</div>
        	
    	</div>
        <div class="col col-12 box p2 my2">
            <form action="/features/newproperty.php" method="POST" accept-charset="utf-8" class="">
            	<div class="col col-4 left pr2">
            		<input type="text" name="address" placeholder="Adresse" id="username" class="col col-12 left"/>
                	<textarea name="biography" placeholder="Beskrivelse" id="username" class="col col-12 left"></textarea>
            	</div>
                <div class="col col-8 left">
                	<input type="text" name="town" placeholder="Postnummer" id="username" class="col col-4 left mr2"/>
                	<input type="text" name="area" placeholder="Areal (kvm)" id="username" class="col col-3 left mr2"/>
                	<input type="number" name="rooms" placeholder="Antal rum" id="username" class="col col-4 left"/>
                	<input type="text" name="rent" placeholder="Husleje" id="username" class="col col-4 left mr2"/>
                	<input type="number" name="construction_year" placeholder="Opførsels år" id="username" min="1900" max="2018" class="col col-3 left mr2"/>
                	<select name="type" class="col col-4 left">
                		<option disabled="" selected="" hidden>Type</option>
                    	<option>Lejlighed</option>
                    	<option>Værelse</option>
                	</select>
                	<input type="file" name="main_image" value="" id="main_image" class="col col-6"/>
                </div>
				<div class="clearfix"></div>
                <input type="submit" name="submit" value="Tilføj ejendom" id="submit" class=""/>
            </form>
        </div>
    </div>
</div>