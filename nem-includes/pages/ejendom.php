<?php
include "nem-includes/class/classes.php";

$town = (is_numeric($_GET["town"])) ? $_GET["town"] : 0;
$address = $_GET["address"];

if (!$town){
    $_SESSION["error"] = "Der skete en fejl, prøv igen!";
} else{
    $db = Database::Instance()->db;
    $db->where("address", $address);
    $db->where("town", $town);
    $result = $db->get("property");

    if (empty($result)) {
        $_SESSION["error"] = "Vi kender ikke noget til det lejemål!";
        Browser::LateRedirect("home");
    }
    $property = new Property($result[0]["id"]);

    $strLejer = (sizeof($property->tenant->name) > 1) ? "lejere" : "lejer";
}
?>
<div class="container mt2">
		<div class="col col-2 mt1 sm-col-12 mt-col-3">
			<div class="col col-11 mr1 sm-col-12">
				<ul class="under_menu">
					<li><a onclick="toggle('#hjem')">- Oversigt</a></li>
					<li><a onclick="toggle('#lejere')">- <?= ucfirst($strLejer)?></a></li>
					<li><a onclick="toggle('#billeder')">- Billeder</a></li>
					<li><a onclick="toggle('#support')">- Support</a></li>
					<li><a onclick="toggle('#okonomi')">- Økonomi</a></li>
					<li><a onclick="toggle('#kontrakter')">- Kontrakter</a></li>
				</ul>
			</div>
		</div>
		<div class="col-right col-10 mt1 sm-col-12 mt-col-9">
			<!-- Lejemål start -->
				<div class="box col col-12 mb1 pb2">
					<div class="col col-12 ejendom_oversigt">
						<div class="col col-2 sm-col-12 mt-col-4">
							<a href="ejendom"><img src="/nem-includes/images/hus.png" alt="" class=""/></a>
						</div>
						<div class="col col-10  sm-col-12 mt-col-8" style="position: relative">
							<div id="mapid_ejendom" class="col col-12"></div>
							<div class="pt2 pr2 pl2" style="position: absolute; z-index: 10; top: 0px;">
								<h3 class="address"><?=$property->address?></h3>
								<p class="mt1 mb2"><?=$property->biography?></p>
							</div>
						</div>
					
						<div class="col-12 left ejendom_menu">
							<ul>
								<li>
									<a href="">Rediger lejemål</a>

                                    <?php if ($property->status == PropertyStatus::OCCUPIED) {
                                        // Kontakt kun, hvis der er lejer?>
                                    <a href="">Kontakt <?= $strLejer?></a>
                                    <?php } else {?>
                                        <a href="">Udlej lejemål</a>
                                    <?php
                                    }?>
								</li>
							</ul>
						</div>
					
					</div>
					<div class="col-3 p2 ejendom right col-right sm-col-12 mt-col-5">
							<div class="lejemal_info">
								<div class="col col-11">
								  	<p class="<?=$property->StatusColor()?>"><span>Status: <?=$property->Status()?></span></p>
								</div>
								<div class="col col-1 right">
									<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="lejemal_info ">
								<div class="col col-11">
									<p><span>Kvm:</span> <?=$property->area?></p>
								</div>
								<div class="col col-1 right">
									<i class="fa fa-home" aria-hidden="true"></i>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="lejemal_info">
								<div class="col col-11">
									<p><span>Værelser:</span> <?=$property->rooms?></p>
								</div>
								<div class="col col-1 right">
									<i class="fa fa-bed" aria-hidden="true"></i>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="lejemal_info">
								<div class="col col-11">
									<p><span>Husleje:</span> <?=$property->rent?>,-</p>
								</div>
								<div class="col col-1 right">
									<i class="fa fa-money" aria-hidden="true"></i>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="lejemal_info">
								<div class="col col-11">
									<p><span>By:</span> <?=Utils::City($property->town)?></p>
								</div>
								<div class="col col-1 right">
									<i class="fa fa-map" aria-hidden="true"></i>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="lejemal_info">
								<div class="col col-11">
									<p><span>Type:</span> <?=$property->type?></p>
								</div>
								<div class="col col-1 right">
									<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="lejemal_info">
								<div class="col col-11">
                                    <?php
                                    $strName = str_replace(", ", "<br>", $property->tenant->Name());
                                    ?>
									<p><span><?= ucfirst($strLejer)?>:</span> <?= $strName?></p>
								</div>
								<div class="col col-1 right pr1">
									<i class="fa fa-user-circle" aria-hidden="true"></i>
								</div>
								<div class="clearfix"></div>
							</div>
					</div>





					<div class="col col-9 p2 sm-col-12 mt-col-7" id="hjem">
						<div class="col col-4 p1 sm-col-12 mt-col-12 mt-col-fix">
							<h3 class="text-center">Vand</h3>
							<img src="http://via.placeholder.com/150x150" alt="" />
						</div>
						<div class="col col-4 p1 sm-col-12 mt-col-12 mt-col-fix">
							<h3 class="text-center">EL</h3>
							<img src="http://via.placeholder.com/150x150" alt="" />
						</div>
						<div class="col col-4 p1 sm-col-12 mt-col-12 mt-col-fix">
							<h3 class="text-center">Varme</h3>
							<img src="http://via.placeholder.com/150x150" alt="" />
						</div>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1905.0942105968338!2d10.381020811363895!3d55.40072901788334!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x464cdffc09b51cd1%3A0x6f5efd077fbfda0b!2s%C3%98stre+Stationsvej+13%2C+5000+Odense+C!5e0!3m2!1sda!2sdk!4v1512682371246" width="592" height="150" frameborder="0" style="border:0; margin-left: 8px; margin-top:8px;"></iframe>
					</div>













                    <div id="lejere" class="col col-9 p2 sm-col-12 mt-col-7" style="display: none;">
                        <?php
                            if (empty($property->tenant->name))
                                echo "Der er ingen lejere til denne ejendom.";
                            else {
                        ?>
                        <div class="col col-12 box">
                            <div class="col col-3 p1"><p class="font-w3">Navn</p></div>
                            <div class="col col-3 p1"><p class="font-w3">Email</p></div>
                            <div class="col col-4 p1"><p class="font-w3">Telefon</p></div>
                            <div class="col col-1 pl1 py1"><p class="font-w3">Status</p></div>
                        </div>
                        <div class="col col-12 box mt1">
                            <div class="ticket">
                                <?php
                                $tenant = (array)$property->tenant;
                                for ($i = 0; $i < sizeof($tenant["name"]); $i++){
                                    ?>
                                        <div class="col col-3 p1"><p><?= $tenant["name"][$i]  ?></p></div>
                                        <div class="col col-3 p1"><p><?= $tenant["email"][$i] ?></p></div>
                                        <div class="col col-3 p1"><p><?= Utils::PrintPhone($tenant["phone"][$i]) ?></p></div>
                                        <div class="col col-1 pl1 py1"><p class="green">Aktiv</p></div>
                                        <div class="clearfix"></div>

                                <?php }//end for ?>
                            </div>
                        </div>
                        <?php } // end else?>
                    </div>















                    <div id="billeder" class="col col-9 p2 sm-col-12 mt-col-7" style="display: none;">
                        Der er ikke knyttet nogle billeder til denne ejendom.
                    </div>


                    <div id="support" class="col col-9 p2 sm-col-12 mt-col-7" style="display: none;">
                        <?php
                        $support = new Ticket(null, $property->id);

                        if (empty($support->id)){
                            echo "Der er ikke oprettet nogle support sager.";
                        }else {
                        ?>
                        <div class="col col-12 box">
                            <div class="col col-4 p1"><p class="font-w3">Emne</p></div>
                            <div class="col col-2 p1"><p class="font-w3">Status</p></div>
                            <div class="col col-3 p1"><p class="font-w3">Seneste svar</p></div>
                        </div>
                        <div class="col col-12 box mt1">
                            <?php
                            for ($i = 0; $i < sizeof($support->id); $i++) {
                                $status = $support->status[$i];
                                ?>
                                <div class="tickets">
                                    <div class="col col-4 p1 ticket_subject"><p><a
                                                    href="/ticket/<?= $support->id[$i] ?>"><?= $support->subject[$i] ?></a>
                                        </p>
                                    </div>
                                    <div class="col col-2 p1"><p
                                                class="<?= Ticket::StatusColor($status) ?>"><?= Ticket::PrintStatus($status) ?></p>
                                    </div>
                                    <div class="col col-3 p1"><p><?= Utils::time_ago($support->time[$i]) ?></p></div>
                                    <div class="clearfix"></div>
                                </div>
                                <?php
                            } //for end?>
                        </div>
                            <a href="/nysag/<?= $property->town .'/'.$property->address?>">Opret ny sag</a>

                        <?php
                        } //end else ?>

                    </div>





                    <div id="okonomi" class="col col-9 p2 sm-col-12 mt-col-7" style="display: none;">
                        Ingen økonomi data
                    </div>




                    <div id="kontrakter" class="col col-9 p2 sm-col-12 mt-col-7" style="display: none;">
                        Der er ikke knyttet nogen kontrakter til denne ejendom.
                    </div>
				</div>
			<!-- Lejemål slut -->
		</div>
		<div class="clearfix"></div>
	</div>
