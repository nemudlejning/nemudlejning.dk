<?php
include "nem-includes/class/classes.php";

$owner = new UserInfo($_SESSION["id"]);

$tickets = $owner->SupportTickets();
foreach ($tickets as $ticket) {
    $numTickets[] = sizeof($ticket->id);
}

//TODO: Counting status
//TODO: new Ticket() gets called twice
/*
 * after
 *                 foreach ($tickets as $ticket){
                    for ($i = 0; $i < sizeof($ticket->id); $i++) {
 */

?>

<div class="container">
		<div class="col col-4 sm-col-10">
			<div class="col col-11 box mt2 mr1 sm-col-fix text-center">
				<h3 class="status-title p2">Nye sager</h3>
				<div class="p2 mt1">
					<h1 class="text-center font-w1"><?= isset($count["Åben"]) ? $count["Åben"] : 0?></h1>
				</div>
			</div>
		</div>
		<div class="col col-4 sm-col-10">
			<div class="col col-12 box mt2 sm-col-fix text-center">
				<h3 class="status-title p2">Nye svar</h3>
				<div class="p2 mt1">
					<h1 class="text-center font-w1"><?= isset($count["Kunde svar"]) ? $count["Kunde svar"] : 0?></h1>
				</div>
			</div>
		</div>
		<div class="col col-4 sm-col-10">
			<div class="col-right col-11 box mt2 ml1 sm-col-fix text-center">
				<h3 class="status-title p2">Ubesvaret</h3>
				<div class="p2 mt1">
					<h1 class="text-center font-w1"><?= isset($count["Ubesvaret"]) ? $count["Ubesvaret"] : 0?></h1>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="container mt2">
		<div class="col col-2 mt1">
			<div class="col col-11 mr1">
				<?php
				include 'nem-includes/pages/side_menu.php';
				?>
			</div>
		</div>

		<div class="col-right col-10 mt1">
			<div class="col col-12 box">
				<div class="col col-4 p1"><p class="font-w3">Emne</p></div>
				<div class="col col-3 p1"><p class="font-w3">Indsender</p></div>
				<div class="col col-2 p1"><p class="font-w3">Status</p></div>
				<div class="col col-3 p1"><p class="font-w3">Seneste svar</p></div>
			</div>
			<div class="col col-12 box mt1">
                <?php
                foreach ($tickets as $ticket){
                    for ($i = 0; $i < sizeof($ticket->id); $i++) {
                        $tick = new Ticket($ticket->id[$i]);

                        $status = $ticket->status[$i];
                        ?>

                        <div class="tickets">
                            <div class="col col-4 p1 ticket_subject"><p><a href="ticket/<?= $ticket->id[$i]?>"><?= $ticket->subject[$i] ?></a></p>
                            </div>

                            <?php
                                $sender = new UserInfo($tick->FirstReply()["respond_id"]);
                                $lastReplyTime = $tick->LastReply()["time"];

                                $strName = str_replace(', ', '<br>', $sender->Name());
                            ?>
                            <div class="col col-3 p1"><p><?= $strName ?></p></div>

                            <div class="col col-2 p1"><p class="<?= Ticket::StatusColor($status) ?>"><?= Ticket::PrintStatus($status) ?></p></div>
                            <div class="col col-3 p1"><p><?= Utils::time_ago($lastReplyTime) ?></p></div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                    } //for end
                } //foreach end?>

			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	