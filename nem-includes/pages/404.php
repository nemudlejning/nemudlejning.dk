<div class="container">
    <div class="float-none col col-4 mx-auto my3 sm-col-11 mt-col-8">
        <div class="col col-12 box p2 my2">
            <img src="images/nemudlejning_404.jpg" alt="" />
            <p class="text-center mt3">Siden du forsøger, at tilgå findes ikke!</p>
            <a href="/"><button class="mx-auto button mt2">Tilbage til hjem</button></a>
        </div>
    </div>
</div>