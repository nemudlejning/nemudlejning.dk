<?php
include "nem-includes/class/classes.php";
$user = new UserInfo($_SESSION["id"]);
$db = Database::Instance()->db;

$tenants = $user->Tenants();
?>
	<div class="container mt2">
		<div class="col col-2 mt1">
			<div class="col col-11 mr1">
				<?php
				include 'nem-includes/pages/side_menu.php';
				?>
			</div>
		</div>
		<div class="col-right col-10 mt1">
            <?php
                if (isset($tenants)){
            ?>
                    <div class="col col-12 box">
                        <div class="col col-3 p1"><p class="font-w3">Navn</p></div>
                        <div class="col col-3 p1"><p class="font-w3">Email</p></div>
                        <div class="col col-4 p1"><p class="font-w3">Ejendom</p></div>
                        <div class="col col-1 pl1 py1"><p class="font-w3">Status</p></div>
                    </div>
                    <div class="col col-12 box mt1">
                        <?php
                        foreach ($tenants as $propertyid => $value) {

                            $strName = str_replace(", ", "<br>", $value->Name());
                            $strEmail = str_replace(", ", "<br>", $value->Email());
                            ?>
                            <div class="ticket">
                                <div class="col col-3 p1"><p><?= $strName ?></p></div>
                                <div class="col col-3 p1"><p><?= $strEmail ?></p></div>

                                <?php $property = new Property($propertyid); ?>
                                <div class="col col-4 p1"><p><?=$property->address ?></p></div>
                                <div class="col col-1 pl1 py1"><p class="green">Aktiv</p></div>
                                <div class="clearfix"></div>
                            </div>
                            <?php
                        } // end for
                ?>

            </div>
        </div>
        <?php
        } // end if
        else {?>
        Du har endnu ingen lejere.
        <?php
        }
        ?>
		<div class="clearfix"></div>
	</div>
	