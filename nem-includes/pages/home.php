<?php include("nem-includes/class/classes.php");

Database::Instance()->db->where("owner", $_SESSION["id"]);
$data = Database::Instance()->db->connection("slave")->get("property");

$income = array("expense" => 0, "profit" => 0);
foreach ($data as $value) {
    if ($value["status"] == PropertyStatus::FREE)
        $income["expense"] += $value["rent"];
    else
        $income["profit"] += $value["rent"];
}
$user = new UserInfo($_SESSION["id"]);

$statusCount = Utils::Count("property", "status", $_SESSION["id"]);
$supportCount = 0;//Utils::Count("support", "status", $_SESSION["id"]);
?>
<div class="container">
		<div class="col col-4 sm-col-10 ">
			<div class="col col-11 box mt2 mr1 sm-col-fix status-box">
				<h2 class="text-center status-title p2">Status</h2>
				<div class="p2 mt2">
					<p>Udlejet lejemål <span class="right green"><?= (isset($statusCount["Udlejet"])) ? $statusCount["Udlejet"] : 0 ?></span></p>
					<p>Ledige lejemål <span class="right red"><?= (isset($statusCount["Ledig"])) ? $statusCount["Ledig"] : 0?></span></p>
				</div>
			</div>
		</div>
		<div class="col col-4 sm-col-10 status-box">
			<div class="col col-12 box mt2 sm-col-fix status-box">
				<h2 class="text-center status-title p2">Økonomi</h2>
				<div class="p2 mt2">
					<p>Indtægter <span class="right green"><?= $income["profit"] ?> DKK</span></p>
					<p>Udgifter <span class="right red"><?= $income["expense"] ?> DKK</span></p>
				</div>
			</div>
		</div>
		<div class="col col-4 sm-col-10 status-box">
			<div class="col-right col-11 box mt2 ml1 sm-col-fix text-center status-box">
				<h2 class="text-center status-title p2">Support</h2>
				<div class="p2 mt2">	
					<div class="col-4 col">
						<h3><?= isset($supportCount["Åben"]) ? $supportCount["Åben"] : 0?></h3>
						<p>Nye sager</p>
					</div>
					<div class="col-4 col">
						<h3><?= isset($supportCount["Kunde svar"]) ? $supportCount["Kunde svar"] : 0?></h3>
						<p>Nye svar</p>
					</div>
					<div class="col-4 col">
						<h3><?= isset($supportCount["På hold"]) ? $supportCount["På hold"] : 0?></h3>
						<p>På hold</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="container mt2">
		<div class="col col-2 mt1">
			<div class="col col-11 mr1">
				<?php
				include 'nem-includes/pages/side_menu.php';
				?>
			</div>
		</div>
		<div class="col-right col-10 mt1">
            <?php
                $data = $user->Properties();
                if (isset($data[0])) {
                    foreach ($data as $value) {
                        ?>

                        <!-- Lejemål start -->
                        <div class="box col col-12 mb1">
                            <div class="col col-2">
                                <a href="ejendom"><img src="nem-includes/images/hus.png" alt="" class=""/></a>
                            </div>
                            <div class="col col-8 pt1 pl2">
                                <?php $link = $value->town . "/" .urlencode($value->address);?>
                                <h3><a href="ejendom/<?=$link?>" class="address"><?= $value->address ?></a></h3>
                                <div class="col col-12">
                                    <div class="lejemal_info">
                                        <p><span><?= $value->area ?></span> m<sup>2</sup></p>
                                    </div>
                                    <div class="lejemal_info">
                                        <p><span><?= $value->rooms ?></span> Værelser</p>
                                    </div>
                                    <div class="lejemal_info">
                                        <p>Husleje <span><?= $value->rent ?>,-</span></p>
                                    </div>
                                    <div class="lejemal_info">
                                        <?php $color = ($value->status == "Ledig") ? "red" : "green"; ?>
                                        <p class="<?= $value->StatusColor() ?>"><span><?= $value->Status() ?></span></p>
                                    </div>
                                    <div class="lejemal_info">
                                        <p>By <span><?= Utils::City($value->town)?></span></p>
                                    </div>
                                    <div class="lejemal_info">
                                        <p>Type <span><?= $value->type ?></span></p>
                                    </div>
                                    <div class="lejemal_info">
                                        <?php
                                            $tenant = new UserInfo(null, $value->id);
                                            $strName = str_replace('<br>', ', ',$tenant->Name());
                                        ?>
                                        <p>Lejer <span><?= $strName?></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-2">
                                <ul class="under_menu">
                                    <li><a href="">Kontrakter</a></li>
                                    <li><a href="">Regninger</a></li>
                                    <li><a href="">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Lejemål slut -->
                    <?php } //slut loop
                } //slut if data
                else { ?>
                    Du har endnu ikke oprettet en ejendom.
                <?php } //slut else ?>
            <a href="nyejendom">Opret ejendom</a>
		</div>
		<div class="clearfix"></div>
	</div>