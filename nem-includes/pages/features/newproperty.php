<?php
include "../../class/classes.php";
session_start();
if (isset($_SESSION["id"])
    && isset($_POST["address"])
    && isset($_POST["town"])
    && isset($_POST["biography"])
    && isset($_POST["area"])
    && isset($_POST["rooms"])
    && isset($_POST["rent"])
    && isset($_POST["type"])
    && isset($_POST["construction_year"])){

    // Checks input
    // Adressen skal være 0 < adresse < 35
    $data["address"] = (CheckInput::Address($_POST["address"])
            && strlen($_POST["address"]) > 0
            && strlen($_POST["address"]) < 35)
            ? $_POST["address"] : "";

    // Postnummeret skal eksisterer
    // 1000 < postnr < 5000
    $data["town"] = (CheckInput::Integer($_POST["town"])
            && $_POST["town"] > 1000
            && $_POST["town"] < 5000)
            ? $_POST["town"] : "";

    // Biografien skal være: bio < 2500
    $data["biography"] = (CheckInput::String($_POST["biography"])
            && strlen($_POST["biography"]) < 2500)
            ? $_POST["biography"] : "";

    // Skal have et areal > 5
    $data["area"] = (CheckInput::Integer($_POST["area"])
            && $_POST["area"] > 5)
            ? $_POST["area"] : "";

    // Mere end et rum
    $data["rooms"] = (CheckInput::Integer($_POST["rooms"])
            && $_POST["rooms"] > 0)
            ? $_POST["rooms"] : "";

    // Husleje skal være positivt heltal
    $data["rent"] = (CheckInput::Integer($_POST["rent"])
            && $_POST["rent"] > 0)
            ? $_POST["rent"] : "";

    // Typen skal eksisterer
    $data["type"] = (CheckInput::PropertyType($_POST["type"]))
            ? $_POST["type"] : "";

    // Byggeår skal være senere end 1800 < byggeår < 2017
    $data["construction_year"] = (CheckInput::Integer($_POST["construction_year"])
            && $_POST["construction_year"] > 1800
            && $_POST["construction_year"] < date("Y"))
            ? $_POST["construction_year"] : "";

    $data["status"] = "Ledig";
    $data["owner_id"] = $_SESSION["id"];

    if (in_array("", $data)){       // Hvis nogle af de øvre check ikke passer => fejl
        $_SESSION["error"] = "Der skete en fejl ved oprettelse af ejendom!";
        Browser::Redirect("nyejendom");
    }

    $property = new Property(null);
    if ($property->Create($data)){
        $_SESSION["message"] = "Din bolig er oprettet!";
        Browser::Redirect("home");
    }

    $_SESSION["error"] = "Adressen er allerede oprettet!";
    Browser::Redirect("nyejendom");
}
$_SESSION["error"] = "Du er enten ikke logget ind ellers mangler der nogle felter!";
Browser::Redirect("nyejendom");