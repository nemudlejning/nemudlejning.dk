<?php
include "../../class/classes.php";
session_start();
if (!(isset($_POST["name"]) && isset($_POST["phone"]) && isset($_POST["email"]) && isset($_POST["password"])
    && ($user = new User($_POST["email"], $_POST["password"]))
    && $user->Create($_POST["name"], $_POST["phone"]) && $user->Login())
    )
    $_SESSION["error"] = "Der skete en fejl ved registration!";

$_SESSION["message"] = (isset($_SESSION["error"])) ? "" : "Velkommen til Nemudlejning.dk, ".$user->GetName()."!";
Browser::Redirect("home");