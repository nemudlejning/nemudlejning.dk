<?php
include "../../class/classes.php";
session_start();
/**
 * Check if password and email isset.
 * Check and sanitize password and email,
 * Check if login passed
 *
 * Else throw an error
 */

if (
!(isset($_POST["password"])
    && isset($_POST["email"])
    && ($user = new User($_POST["email"], $_POST["password"]))
    && ($user->Login()))
)
        $_SESSION["error"] = "Der skete en fejl ved login!";

$_SESSION["message"] = (isset($_SESSION["error"])) ? "" : "Velkommen!";

$page = (isset($_SESSION["redirect"])) ? $_SESSION["redirect"] : "home";
$_SESSION["redirect"] = [];
Browser::Redirect($page);