<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Kontrolpanel - Nem Udlejning</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="/css/default.css" type="text/css" media="screen">
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
	<link rel="stylesheet" href="/css/media_queries.css" type="text/css" media="screen">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />
	<script src="https://use.fontawesome.com/02c1f5e327.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="/js/script.js"></script>
	<link rel="apple-touch-icon" sizes="180x180" href="/nem-includes/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/nem-includes/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/nem-includes/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="/nem-includes/images/favicon/manifest.json">
	<link rel="mask-icon" href="/nem-includes/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
</head>
<body>
	<header>
		<div class="container">
			<nav>
				<div class="col col-3 logo">
					<a href="/home"><img src="/images/nem_logo_6.png" alt="" /></a>
				</div>
				<ul>
					<li><a href="/home">Hjem</a></li>
					<li><a href="/help">Hjælp</a></li>

					<?php if (isset($_SESSION["id"])){?>
                    <li><a href="/faktura">Faktura</a></li>
					<li><a href="/indstillinger">Indstillinger</a></li>
                    <?php }
                            $text = (isset($_SESSION["id"]) ? "Logud" : "Log ind");
                            $link = (isset($_SESSION["id"]) ? "logout" : "login");?>
					<li><a href="<?=$link?>"><?=$text?></a></li>
					<li><a href="" class="icon">&#9776;</a></li>
				</ul>
				<div class="clearfix"></div>
			</nav>
		</div>
	</header>
<div class="container">
<?php
if (isset($_SESSION["error"]))
    printf("<div class='feedback box'>
		<i class='fa fa-times right fa-4 closebtn' aria-hidden='true'></i>
		<h3 class='red text-center'>Fejl: %s</h3>
	</div>", $_SESSION["error"]);
unset($_SESSION["error"]);

if (isset($_SESSION["message"]) && $_SESSION["message"] != "")
    printf("<div class='feedback box'>
		<i class='fa fa-times right fa-4 closebtn' aria-hidden='true'></i>
		<h3 class='green text-center'>Besked: %s</h3>
	</div>", $_SESSION["message"]);
unset($_SESSION["message"]);

?>
</div>