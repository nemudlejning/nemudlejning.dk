<?php
include "nem-includes/class/classes.php";
/**
 * Input checks
 */

// is input valid?
$town = (is_numeric($_POST["town"])) ? CheckInput::Sanitize($_POST["town"]) : 0;
$address = CheckInput::Sanitize($_POST["address"]);

$message = CheckInput::Sanitize($_POST["message"]);
$subject = CheckInput::Sanitize($_POST["subject"]);

$err = false;
if (!$town)
    $err = true;

if (!isset($message)    ||     !isset($subject) ||     !isset($address))
    $err = true;

if ($err){
    $_SESSION["err"] = "Der gik noget galt under oprettelsen af din sag!";
    Browser::LateRedirect("home");
    die();
}

// does the property exists?
$db = Database::Instance()->db;
$db->where("address", $address);
$db->where("town", $town);
$result = $db->get("property");

if (empty($result)) {
    $_SESSION["error"] = "Vi kender ikke noget til det lejemål!";
    Browser::LateRedirect("home");
    die();
}

// Does this user have a relation to the property?
$property = new Property($result[0]["id"]);
$user = new UserInfo($_SESSION["id"]);

$properties = $user->Properties();

$err = true;
foreach ($properties as $prop){
    $err = ($prop->id == $property->id) ? false : $err;
}
if ($err){
    $_SESSION["error"] = "Du er ikke i relation til dette lejemål";
    Browser::LateRedirect("home");
    die();
}
/**
 *  End inputcheck
 */
$ticket = Ticket::Create($subject, $message, $property->id);
Browser::LateRedirect("ticket/".$ticket->id[0]);