<?php
include "nem-includes/class/classes.php";
$id = $_GET["id"];
$ticket = new Ticket($id);

$tenant = $ticket->tenant[0];
$user = new UserInfo($_SESSION["id"]);

$conversation = $ticket->conversation[0];

if (empty($id))
    Browser::LateRedirect("home");
?>

	<div class="container mt2">
		<div class="col col-2 mt1">
			<div class="col col-11 mr1">
				<?php
				include 'nem-includes/pages/side_menu.php';
				?>
			</div>
		</div>
		<div class="col col-right col-10 mt1">
			<div class="box col col-12 mb1 ticket_lejer_box ticket_svar">
				<div class="col col-8 p2">
					<h2>Sag #<?=$ticket->id[0]?> - <?= $ticket->subject[0]?></h2>
					<form action="/nytsvar" method="POST" accept-charset="utf-8" class="mt2">
<textarea name="message" class="col col-12">


Med venlig hilsen
<?=$user->Name()?></textarea>
                        <input type="hidden" name="ticket_id" value="<?= $ticket->id[0]?>">
						<input type="submit" name="submit" value="Send" id="submit" class="button left"/>
						<label class="select">
						<select name="" id="" class="ticket_status">
							<option class="green">Åben</option>
							<option >Kundesvar</option>
							<option class="orange">Besvaret</option>
							<option class="blue">På hold</option>
							<option class="red">Lukket</option>
						</select>
						</label>
					</form>
				</div>
				<div class="col col-4 px2 py1 ejendom right ticket_info_udlejer">
						<h2>Modtager</h2>
						<div class="lejemal_info mt2">
							<p class="green"><span>Status: Aktiv</span></p> 
						</div>
						<div class="lejemal_info ">
							<p><span>Navn:</span> <?= $tenant->Name()?></p>
						</div>
						<div class="lejemal_info">
							<p><span>Email:</span> <?= $tenant->Email()?></p>
						</div>
						<div class="lejemal_info">
							<p><span>Telefon:</span> <?= $tenant->Phone()?></p>
						</div>
				</div>
			</div>
			
			
			<!-- Ticket start -->
            <?php
            foreach (array_reverse($conversation) as $value) {
                $replyer = new UserInfo($value["respond_id"]);
                $time = strftime('%A d. %e %B, Kl. %H:%M', $value["time"]);
                $message = nl2br($value["message"]);

                $isTenant = $replyer->role[0] == Roles::TENANT;
                $divClass[1] = ($isTenant) ? "ticket_lejer_box" : "ticket_udlejer_box";
                $divClass[2] = ($isTenant) ? "col ticket_info_lejer" : "col-right ticket_info_udlejer right";
                ?>
                <div class="box col col-12 mb1 <?=$divClass[1]?>">
                    <div class="col-4 p2 <?=$divClass[2]?>">
                        <p><?=$replyer->Name()?></p>
                        <p class="message_sent"><?= $replyer->Role()?></p>
                    </div>
                    <div class="col col-8 p2">
                        <p class="message_sent">Sendt <?= $time?></p>
                        <p class="ticket_message"><?= $message?></p>
                    </div>
                </div>

                <?php
                $replyer = null;
            } // end loop
            ?>

			<!-- Ticket slut -->
		</div>
		<div class="clearfix"></div>
	</div>