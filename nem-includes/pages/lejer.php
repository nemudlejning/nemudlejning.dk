<div class="container mt2">
	<div class="col col-2 mt1">
			<div class="col col-11 mr1">
				<?php
				include 'nem-includes/pages/side_menu.php';
				?>
			</div>
	</div>
	<div class="col-right col-10 mt1">
		<div class="col col-12 ejendom_oversigt">
						
					</div>
				
					
					<!-- Oversigt start -->
					<div class="col-12 left ejendom_menu mb2">
						<ul>
							<li>
								<a href="">Rediger lejer</a>
                                <a href="">Opret ny sag</a>
                                <a href="">Login som lejer</a>
							</li>
						</ul>
					</div>
					<div class="box col col-12 mb1">
					<div class="col col-4 sm-col-12 mt-col-7 ticket_lejer_side" id="hjem">
						<h2 class="lejer_title_oversigt">Oversigt</h2>
					</div>
					<div class="col-4 ejendom right col sm-col-12 mt-col-5">
						<h2 class="lejer_title_oversigt">Lejer information</h2>
						<div class="p2">
							<div class="lejemal_info">
								  	<p class="">Navn:<span> Morten Zimmer</span></p>
							</div>
							<div class="lejemal_info">
								  	<p class="">Alder:<span> 21</span></p>
							</div>
							<div class="lejemal_info">
								  	<p class="">Køn:<span> Ja</span></p>
							</div>
							<div class="lejemal_info">
								  	<p class="">Lejer siden:<span> 08/07-2016</span></p>
							</div>
							<div class="lejemal_info">
								  	<p class="">Total indtægt:<span> 48.666,-</span></p>
							</div>
							</div>
					</div>
					<div class="col-4 ejendom right col sm-col-12 mt-col-5">
						<h2 class="lejer_title_oversigt">Kontakt information</h2>
							<div class="p2">
							<div class="lejemal_info">
								  	<p class="">Email:<span> Mortenz@live.dk</span></p>
							</div>
							<div class="lejemal_info">
								  	<p class="">Adresse:<span> Søgårdsvej 21</span></p>
							</div>
							<div class="lejemal_info">
								  	<p class="">By:<span> Odense</span></p>
							</div>
							<div class="lejemal_info">
								  	<p class="">Telefon:<span> 60 18 34 39</span></p>
							</div>
							<div class="lejemal_info">
								  	<p class="">Land:<span> Danmark</span></p>
							</div>
							</div>
					</div>
				</div>
				<!-- Oversigt slut -->
				
				 <!-- Lejemål start -->
				 		<h2 class="lejer_title">Mortens lejemål </h2>
                        <div class="box col col-12 mb2">
                            <div class="col col-2">
                                <a href="ejendom"><img src="nem-includes/images/hus.png" alt="" class=""/></a>
                            </div>
                            <div class="col col-8 pt1 pl2">
                                <h3><a href="ejendom/" class="address">Østre Stationsvej 39B 5 sal MF</a></h3>
                                <div class="col col-12">
                                    <div class="lejemal_info">
                                        <p><span>57</span> m<sup>2</sup></p>
                                    </div>
                                    <div class="lejemal_info">
                                        <p><span>2</span> Værelser</p>
                                    </div>
                                    <div class="lejemal_info">
                                        <p>Husleje <span>5940,-</span></p>
                                    </div>
                                    <div class="lejemal_info">
                                        <p class="green"><span>Udlejet</span></p>
                                    </div>
                                    <div class="lejemal_info">
                                        <p>By <span>Odense</span></p>
                                    </div>
                                    <div class="lejemal_info">
                                        <p>Type <span>Lejlighed</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-2">
                                <ul class="under_menu">
                                    <li><a href="">Kontrakter</a></li>
                                    <li><a href="">Regninger</a></li>
                                    <li><a href="">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>
                    <!-- Lejemål slut -->
                    <!-- Support start -->
                    <h2 class="lejer_title">Mortens sager </h2>
                    <div class="clearfix"></div>
                    <div class="col col-12 sm-col-12 mt-col-7 ticket_lejer_side mb2">
						<div class="col col-12 box">
							<div class="col col-4 p1"><p class="font-w3">Emne</p></div>
							<div class="col col-4 p1"><p class="font-w3">Indsender</p></div>
							<div class="col col-2 p1"><p class="font-w3">Status</p></div>
							<div class="col col-2 p1"><p class="font-w3">Seneste svar</p></div>
						</div>
						<div class="col col-12 mt1 box">
                        <div class="tickets">
                            <div class="col col-4 p1 ticket_subject"><p><a href="ticket">scubab</a></p></div>
                            <div class="col col-4 p1"><p>Morten Zimmer<br>Kristian á Brúnni</p></div>
                            <div class="col col-2 p1"><p class="green">Åben</p></div>
                            <div class="col col-2 p1"><p>13 Oct</p></div>
                            <div class="clearfix"></div>
                        </div>
						</div>
					</div>
                    
                    <!-- Support slut -->
	</div>
</div>