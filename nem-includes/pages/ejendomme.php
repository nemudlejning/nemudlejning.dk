		<div class="container mt2">
			<h2 class="text-center">Østre Stationsvej 39B 5 Sal, MF</h2>
			<div class="col col-2 mt1">
				<div class="col col-11 mr1 mb1">
					<img src="nem-includes/images/hus.png" alt="" />
				</div>
				<div class="col col-11 mr1">
					<?php
						include 'nem-includes/pages/side_menu.php';
					?>
				</div>
			</div>
			<div class="col-right col-10 mt1">
				<!-- Lejemål start -->
				<div class="box col col-12 mb1">
					<div class="col col-2">
						<a href="ejendom"><img src="nem-includes/images/hus.png" alt="" class=""/></a>
					</div>
					<div class="col col-8 p2">
							<h3><a href="">Østre Stationsvej 39B 5 SAL, MF</a></h3>
							<div class="col col-12">
								<div class="lejemal_info">
									<p><span>107</span> kvm</p>
								</div>
								<div class="lejemal_info">
									<p><span>3</span> Værelser</p>
								</div>
								<div class="lejemal_info">
									<p><span>9.950</span> Husleje</p>
								</div>
								<div class="lejemal_info">
									<p class="green"><span>Udlejet</span></p>
								</div>
								<div class="lejemal_info">
									<p>By <span>Odense C</span></p>
								</div>
								<div class="lejemal_info">
									<p>Type <span>Lejlighed</span></p>
								</div>
								<div class="lejemal_info">
									<p>Lejer <span>Morten Zimmer</span></p>
								</div>
							</div>
					</div>
					<div class="col col-2">
						<ul class="under_menu">
							<li><a href="">Kontrakter</a></li>
							<li><a href="">Regninger</a></li>
							<li><a href="">Kontakt</a></li>
						</ul>
					</div>
				</div>
				<!-- Lejemål slut -->
				
				<div class="box col col-12 mb1">
					<div class="col col-2">
						<a href="ejendom"><img src="nem-includes/images/hus_2.png" alt="" class=""/></a>
					</div>
					<div class="col col-8 p2">
							<h3><a href="ejendom">Østre Stationsvej 39B</a></h3>
							<div class="col col-12">
								<div class="lejemal_info">
									<p><span>3</span> Lejligheder</p>
								</div>
								<div class="lejemal_info">
									<p><span>21.950</span> Husleje</p> 
								</div>
								<div class="lejemal_info">
									<p class="red"><span>Ledig</span></p>
								</div>
								<div class="lejemal_info">
									<p>By <span>Odense C</span></p>
								</div>
								<div class="lejemal_info">
									<p>Type <span>Boligblok</span></p>
								</div>
							</div>
					</div>
					<div class="col col-2">
						<ul class="under_menu">
							<li><a href="">Kontrakter</a></li>
							<li><a href="">Regninger</a></li>
							<li><a href="">Kontakt</a></li>
						</ul>
					</div>
				</div>
				
				<div class="box col col-12 mb1">
					<div class="col col-2">
						<a href="ejendom"><img src="nem-includes/images/hus.png" alt="" class=""/></a>
					</div>
					<div class="col col-8 p2">
							<h3><a href="ejendom">Søgårdsvej 21</a></h3>
							<div class="col col-12">
								<div class="lejemal_info">
									<p><span>219</span> Kvm2</p>
								</div>
								<div class="lejemal_info">
									<p><span>7</span> Værelser</p>
								</div>
								<div class="lejemal_info">
									<p><span>13.950</span> Husleje</p>
								</div>
								<div class="lejemal_info">
									<p class="green"><span>Udlejet</span></p> 
								</div>
								<div class="lejemal_info">
									<p>By <span>Odense C</span></p>
								</div>
								<div class="lejemal_info">
									<p>Type <span>Hus</span></p>
								</div>
								<div class="lejemal_info">
									<p>Lejer <span>John Doe</span></p>
								</div>
							</div>
					</div>
					<div class="col col-2">
						<ul class="under_menu">
							<li><a href="">Kontrakter</a></li>
							<li><a href="">Regninger</a></li>
							<li><a href="">Kontakt</a></li>
						</ul>
					</div>
				</div>
				
			</div>
			<div class="clearfix"></div>
		</div>
