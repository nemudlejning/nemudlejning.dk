	<footer class="clearfix text-center mt2 mb2">
		&copy; Nemudlejning.dk 2017 - Alle rettigheder forbeholdes - Alpha v 0.5
	</footer>
	<script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
	<script>
		var map = L.map('mapid').setView([51.505, -0.09], 17);
		
		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    	attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(map);
		
		var map_ejendom = L.map('mapid_ejendom').setView([51.505, -0.09], 17);
		
		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    	attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(map_ejendom);
	</script>
</body>
</html>