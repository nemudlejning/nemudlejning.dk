<?php
if (isset($_GET["page"])) {
    session_start();
    $_SESSION['redirect'] = $_GET["page"];
}
?>
<div class="container">
	<div class="float-none col col-4 mx-auto my3 sm-col-11 mt-col-8">
		<div class="col col-12 box p2 my2">
			<h1 class="text-center font-w2">Login</h1>
			<form action="/nem-includes/pages/features/login.php" method="POST" accept-charset="utf-8" class="login">
			  <input type="text" name="email" placeholder="Email" id="username" class="col-10 mx-auto"/>
			  <input type="password" name="password" placeholder="Adgangskode" id="password" class="col-10 mx-auto"/>
			  <input type="submit" name="submit" value="Login" id="submit" class="mx-auto"/>
			</form>
			<p class="text-center mt3"><a href="">Har du glemt dit login?</a></p>
		</div>
	</div>
</div>