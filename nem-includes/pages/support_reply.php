<?php
include "nem-includes/class/classes.php";

$user = new UserInfo($_SESSION["id"]);
$tickets = $user->SupportTickets();
/**
 * Input checks
 */
$message = CheckInput::Sanitize($_POST["message"]);
$ticket_id = (int) CheckInput::Sanitize($_POST["ticket_id"]);


$err = false;
if (!isset($message)    ||     !isset($ticket_id))
    $err = true;

if (!CheckInput::Integer($ticket_id))
    $err = true;
else {
    foreach ($tickets as $ticket) {
        if (!in_array($ticket_id, $ticket->id))
            $err = true;
    }
}
if ($err){
    $_SESSION["err"] = "Der gik noget galt under besvarelsen af din sag!";
    Browser::LateRedirect("home");
    die();
}

/**
 *  End inputcheck
 */

$ticket = new Ticket($ticket_id);
$ticket->Reply($message);
Browser::LateRedirect("ticket/".$ticket_id);